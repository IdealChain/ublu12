/* stmttree.c, Daniel Achleitner (0926807) */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "stmttree.h"

struct stat *stat_alloc()
{
	struct stat *s = malloc(sizeof(*s));
	MALLOC_CHECK(s)
	memset(s, 0, sizeof(*s));
	
	return s;
}

struct stat *stat_add(struct stat *list, struct stat *s)
{
	/* if list is empty: s gets first item */
	if (!list)
		return s;

	/* add new element at the end of the list */
	struct stat *l = list;
	
	while (l->next)
		l = l->next;
	
	l->next = s;
	return list;
}

struct stat *stat_add_return(struct stat *list, struct node *expr)
{
	assert(expr);
	
	struct stat *s = stat_alloc();
	s->type = statRETURN;
	s->expr = expr;
	
	return stat_add(list, s);
}

struct stat *stat_add_label(struct stat *list, struct sym *label)
{
	struct stat *s = stat_alloc();
	s->type = statLABEL;
	s->data.label = label; 
	
	return stat_add(list, s);
}

struct stat *stat_add_goto(struct stat *list, struct sym *label)
{
	struct stat *s = stat_alloc();
	s->type = statGOTO;
	s->data.label = label;  /* may be NULL (if label is not defined -> check traversal not run yet) */
	
	return stat_add(list, s);
}

struct stat *stat_add_if(struct stat *list, struct node *expr, struct stat *body, struct sym *scope_vars)
{
	assert(expr);
	
	struct stat *s = stat_alloc();
	s->type = statIF;
	s->expr = expr;
	s->data.body = body;
	s->scope_vars = scope_vars;
	
	return stat_add(list, s);
}

struct stat *stat_add_vardef(struct stat *list, struct node *expr, struct sym *var)
{
	assert(expr);
	
	struct stat *s = stat_alloc();
	s->type = statVARDEF;
	s->expr = expr;
	s->data.var = var; /* may be NULL (if variable is not defined -> check traversal not run yet) */
	
	return stat_add(list, s);
}

struct stat *stat_add_memass(struct stat *list, struct node *expr, struct node *adr)
{
	assert(expr);
	assert(adr);
	
	struct stat *s = stat_alloc();
	s->type = statMEMASS;
	s->expr = expr;
	s->data.adr = adr;
	
	return stat_add(list, s);
}

struct stat *stat_add_eval(struct stat *list, struct node *expr)
{
	assert(expr);
	
	struct stat *s = stat_alloc();
	s->type = statEVAL;
	s->expr = expr;
	
	return stat_add(list, s);
}
